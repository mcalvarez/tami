package application;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import datamodel.dao.IClienteDAO;
import datamodel.dao.IPeliculaDAO;
import datamodel.dao.impl.ClienteDAODummy;
import datamodel.dao.impl.PeliculaDAODummy;
import datamodel.entities.Cliente;
import datamodel.entities.Empleado;
import datamodel.entities.Pelicula;
import datamodel.entities.Tienda;

/**
 * Consultas de ejemplo sobre la BD del videoclub
 */
public class Consultas {
	public static void ejecutar(EntityManager entityManager) {
		IClienteDAO clienteDAO = new ClienteDAODummy();
		IPeliculaDAO peliculaDAO = new PeliculaDAODummy();

		// Crear los clientes
		System.out.println("- Crear el cliente Paco ");
		Cliente nuevoCliente = new Cliente();
		nuevoCliente.setNombre("Paco");
		clienteDAO.add(nuevoCliente);

		// Crear las peliculas
		System.out.println("- Crear la pelicula Alienado");
		Pelicula nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Alienado");
		peliculaDAO.add(nuevaPelicula);

		System.out.println("- Crear la pelicula Desafio parcial");
		nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Desafio parcial");
		peliculaDAO.add(nuevaPelicula);

		System.out.println("- Crear la pelicula Titanico");
		nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Titanico");
		peliculaDAO.add(nuevaPelicula);

		// Alquilar una pelicula
		System.out.println("- Paco alquila las peliculas Alienado y Titanico");
		Cliente cliente = clienteDAO.getByName("Paco");
		Pelicula pelicula = peliculaDAO.getByName("Alienado");
		Pelicula pelicula2 = peliculaDAO.getByName("Titanico");

		Set<Pelicula> peliculas = new HashSet<Pelicula>();
		peliculas.add(pelicula);
		peliculas.add(pelicula2);
		cliente.setAlquila(peliculas);
		clienteDAO.update(cliente);

		// Recuperar los clientes que han alquilado una pelicula
		List<Cliente> clientes = clienteDAO
				.listClientesAlquilanPelicula(pelicula.getId());

		System.out.println("- Listar clientes que han alquilado la pelicula Alienado");
		// Mostramos el listado de clientes
		listResults(clientes);
	}

	public static <T> void listResults(Collection<T> results) {
		for (Object s : results) {
			System.out.println(s.toString());
		}
	}
}
