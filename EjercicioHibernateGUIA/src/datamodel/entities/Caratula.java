package datamodel.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Caratula {
	@Id
	@GeneratedValue
	private int id;
	private String codigo;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Pelicula pelicula;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
