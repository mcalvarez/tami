package datamodel.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Pelicula {
	@Id
	@GeneratedValue
	private int idPelicula;
	private String nombre;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Caratula caratula;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Set<Cliente> alquilada;

	public int getId() {
		return idPelicula;
	}
	
	public Set<Cliente> getAlquilada(){
		return alquilada;
	}
	
	public void setAlquilada(Set<Cliente> ListaClientes){
		this.alquilada=ListaClientes;
	}

	public void setId(int id) {
		this.idPelicula = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Caratula getCaratula() {
		return caratula;
	}

	public void setCaratula(Caratula caratula) {
		this.caratula = caratula;
	}
	
	public String toString(){
		return "Pel�cula: " + idPelicula +", " + nombre;
	}

}
