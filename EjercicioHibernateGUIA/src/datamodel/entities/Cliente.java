package datamodel.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Cliente extends Usuario {
	private int numCliente;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Set<Pelicula> alquila;
	
	public Set<Pelicula> getAlquila(){
		return alquila;
	}
	
	public void setAlquila(Set<Pelicula> ListaPeliculas){
		this.alquila = ListaPeliculas;
	}
	
	public int getNum(){
		return numCliente;
	}
	

}
