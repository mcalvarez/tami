package datamodel.entities;

import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
public class Empleado extends Usuario {
	private int numEmpleado;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Tienda trabaja_en;

	public Tienda getTienda() {
		return trabaja_en;
	}

	public void setTienda(Tienda store) {
		this.trabaja_en = store;
	}

	public int getNum() {
		return numEmpleado;
	}
}