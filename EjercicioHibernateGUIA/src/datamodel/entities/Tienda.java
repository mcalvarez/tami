package datamodel.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Tienda {
	
	@Id
	@GeneratedValue
	private int idTienda;
	private String nombre;
	
	// Pista: Usalo para establecer la relacion!
	@Transient
	private Set<Empleado> empleados;

	public int getId() {
		return idTienda;
	}

	public void setId(int id) {
		this.idTienda = id;
	}

	public void setEmpleados(Set<Empleado> empleados) {
		this.empleados = empleados;
	}

	public Set<Empleado> getEmpleados() {
		return empleados;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String toString() {
		return "Tienda: " + idTienda + ", " + nombre;
	}

}
