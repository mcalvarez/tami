package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import datamodel.dao.IClienteDAO;
import datamodel.entities.Cliente;
import datamodel.entities.Pelicula;

public class ClienteDAODummy implements IClienteDAO {
	private Map<String, Cliente> clientes;
	
	public ClienteDAODummy() {
		clientes = new HashMap<String, Cliente>();
	}
	
	@Override
	public Cliente getByName(String name) {
		return clientes.get(name);
	}

	@Override
	public List<Cliente> findAll() {
		return new ArrayList<Cliente>(clientes.values());
	}

	@Override
	public void add(Cliente cliente) {
		clientes.put(cliente.getNombre(), cliente);
	}

	@Override
	public void update(Cliente cliente) {
		clientes.put(cliente.getNombre(), cliente);
	}

	@Override
	public void delete(Cliente cliente) {
		clientes.remove(cliente);
	}

	@Override
	public List<Cliente> listClientesAlquilanPelicula(int idPelicula) {
		List<Cliente> clientePeliculas = new ArrayList<Cliente>();
		for (Cliente cliente: clientes.values()) {
			if (cliente.getAlquila() != null) {
				Set<Pelicula> peliculas = cliente.getAlquila();
				for (Pelicula pelicula:peliculas) {
					if (pelicula.getId() == idPelicula) {
						clientePeliculas.add(cliente);
						break;
					}
				}
			}
		}
		return clientePeliculas;
	}

}
