package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import datamodel.dao.IPeliculaDAO;
import datamodel.entities.Cliente;
import datamodel.entities.Pelicula;

public class PeliculaDAODummy implements IPeliculaDAO {
	private Map<String, Pelicula> peliculas;
	
	public PeliculaDAODummy() {
		peliculas = new HashMap<String, Pelicula>();
	}
	
	@Override
	public Pelicula getByName(String name) {
		return peliculas.get(name);
	}

	@Override
	public List<Pelicula> findAll() {
		return new ArrayList<Pelicula>(peliculas.values());
	}

	@Override
	public void add(Pelicula pelicula) {
		peliculas.put(pelicula.getNombre(), pelicula);
	}

	@Override
	public void update(Pelicula pelicula) {
		peliculas.put(pelicula.getNombre(), pelicula);
	}

	@Override
	public void delete(Pelicula pelicula) {
		peliculas.remove(pelicula);
	}

}
