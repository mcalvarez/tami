package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.IClienteDAO;
import datamodel.entities.Cliente;

public class ClienteDAO implements IClienteDAO {
	private EntityManager entityManager;

	public ClienteDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Cliente getByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Cliente> findAll() {
		List<Cliente> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager.createQuery("FROM Cliente");
			result = query.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	@Override
	public void add(Cliente cliente) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(cliente);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	@Override
	public void update(Cliente cliente) {
		add(cliente);
	}

	@Override
	public void delete(Cliente cliente) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(cliente); // Pelicula es un objeto Hibernate
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}

	}

	@Override
	public List<Cliente> listClientesAlquilanPelicula(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}
}
