package datamodel.dao;

import java.util.List;

import datamodel.entities.Cliente;

public interface IClienteDAO {
	Cliente getByName(String name);
	List<Cliente> findAll();
	void add(Cliente cliente);
	void update(Cliente cliente);
	void delete(Cliente cliente);
	List<Cliente> listClientesAlquilanPelicula(int idPelicula);
}
