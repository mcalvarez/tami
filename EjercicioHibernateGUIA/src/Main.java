import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import application.Consultas;

public class Main {
	public static void main(String args[]) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("test_unit");
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		
		Consultas.ejecutar(entityManager);
		
		entityManager.close();
		entityManagerFactory.close();
	}
}
