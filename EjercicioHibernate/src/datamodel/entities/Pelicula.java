package datamodel.entities;

import javax.persistence.*;

import java.util.Set;

@Entity
public class Pelicula {
	
	@Id
	@GeneratedValue
	private int idPelicula;
	
	private String nombre;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idCaratula")
	private Caratula caratula;

	@ManyToMany(cascade=CascadeType.ALL, mappedBy="alquila")
	private Set<Cliente> alquilada;

	public int getId() {
		return idPelicula;
	}
	
	public Set<Cliente> getAlquilada(){
		return alquilada;
	}
	
	public void setAlquilada(Set<Cliente> ListaClientes){
		this.alquilada=ListaClientes;
	}

	public void setId(int id) {
		this.idPelicula = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Caratula getCaratula() {
		return caratula;
	}

	public void setCaratula(Caratula caratula) {
		this.caratula = caratula;
	}
	
	public String toString(){
		return "Pel�cula: " + idPelicula +", " + nombre;
	}

}
