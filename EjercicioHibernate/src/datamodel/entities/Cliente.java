package datamodel.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name="idCliente")
public class Cliente extends Usuario {
	
	private int numCliente;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="CLIENTE_PELICULA", joinColumns=@JoinColumn(name="Cliente_ID"),
			inverseJoinColumns=@JoinColumn(name="Pelicula_ID"))
	private Set<Pelicula> alquila;
	
	public Set<Pelicula> getAlquila(){
		return alquila;
	}
	
	public void setAlquila(Set<Pelicula> ListaPeliculas){
		this.alquila = ListaPeliculas;
	}
	
	public int getNum(){
		return numCliente;
	}
	

}
