package datamodel.entities;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@PrimaryKeyJoinColumn(name="idEmpleado")
public class Empleado extends Usuario {	
	private int numEmpleado;
	
	@ManyToOne
	@JoinColumn(nullable=false)
	private Tienda trabaja_en;
	
	public Tienda getTienda(){
		
		return trabaja_en;
	}
	
	public void setTienda(Tienda store){
		this.trabaja_en = store;
	}
	
	public int getNum(){
		return numEmpleado;
	}
	public String toString(){
		return ("El empleado n�mero "+getNum()+"trabaja en "+getTienda()+"\n");
	}

}