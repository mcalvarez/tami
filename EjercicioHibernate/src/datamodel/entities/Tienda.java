package datamodel.entities;

import javax.persistence.*;

import java.util.Set;

@Entity
public class Tienda {
	
	@Id
	@GeneratedValue
	private int idTienda;
	
	private String nombre;
	
	@OneToMany(mappedBy="trabaja_en",fetch=FetchType.EAGER)
	private Set<Empleado> empleados;

	public int getId() {
		return idTienda;
	}
	

	public void setId(int id) {
		this.idTienda = id;
	}
	
	public void setEmpleados(Set<Empleado> empleados){
		this.empleados = empleados;
	}
	
	public Set<Empleado> getEmpleados(){
		return empleados;
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString(){
		return "Tienda: " + idTienda +", " + nombre;
	}

}
