package datamodel.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Usuario {
	
	@Id
	@GeneratedValue
	private int idUsuario;
	
	private String nombre;

	public int getId() {
		return idUsuario;
	}

	public void setId(int id) {
		this.idUsuario = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString(){
		return "Usuario: " + idUsuario +", " + nombre;
	}

}
