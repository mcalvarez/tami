package datamodel.dao;

import java.util.List;

import datamodel.entities.Caratula;

public interface ICaratulaDAO {
	Caratula getByCodigo(String codigo);
	List<Caratula> findAll();
	void add(Caratula caratula);
	void update(Caratula caratula);
	void delete(Caratula caratula);
}
