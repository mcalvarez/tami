package datamodel.dao;

import java.util.List;

import datamodel.entities.Usuario;

public interface IUsuarioDAO {
	Usuario getByName(String name);
	List<Usuario> findAll();
	void add(Usuario usuario);
	void update(Usuario usuario);
	void delete(Usuario usuario);
}
