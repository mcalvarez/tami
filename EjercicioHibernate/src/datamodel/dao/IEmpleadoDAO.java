package datamodel.dao;

import java.util.List;

import datamodel.entities.Empleado;

public interface IEmpleadoDAO {
	Empleado getByName(String name);
	List<Empleado> findAll();
	void add(Empleado empleado);
	void update(Empleado empleado);
	void delete(Empleado empleado);
}
