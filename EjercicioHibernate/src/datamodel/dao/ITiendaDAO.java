package datamodel.dao;

import java.util.List;

import datamodel.entities.Tienda;

public interface ITiendaDAO {
	Tienda getByName(String name);
	List<Tienda> findAll();
	void add(Tienda tienda);
	void update(Tienda tienda);
	void delete(Tienda tienda);
}
