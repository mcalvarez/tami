package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.IClienteDAO;
import datamodel.entities.Cliente;

public class ClienteDAO implements IClienteDAO {

	private EntityManager entityManager;

	public ClienteDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Cliente> listClientesAlquilanPelicula(int idPelicula) {
		List<Cliente> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery(
					"SELECT p FROM Cliente p join p.alquila a WHERE a.idPelicula = "
							+ idPelicula).getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public Cliente getByName(String name) {
		Cliente cliente = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager
					.createQuery("SELECT p FROM Cliente p  WHERE p.nombre = :name");
			query.setParameter("name", name);
			cliente = (Cliente) query.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return cliente;
	}

	public Cliente getById(int id) {
		Cliente cliente = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			cliente = (Cliente) entityManager.createQuery(
					"SELECT p FROM Cliente p  WHERE p.idUsuario = " + id)
					.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return cliente;
	}

	public List<Cliente> findAll() {
		List<Cliente> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p FROM Cliente p")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public void add(Cliente emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de actualizarlo
	public void update(Cliente emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.merge(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de borrarlo
	public void delete(Cliente emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

}
