package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import datamodel.entities.Usuario;

public class UsuarioDAO {
	
	private EntityManager entityManager;
	 
	 public UsuarioDAO(EntityManager entityManager) {
		 this.entityManager = entityManager;
	 }
	
	 public List<Usuario> findAll() {
		 List<Usuario> result = new ArrayList<>();
		 EntityTransaction transaction = entityManager.getTransaction();
		 try {
			 transaction.begin();
			 result = entityManager.createQuery("SELECT p FROM Usuario p").getResultList();
			 transaction.commit();
		 } catch (Exception e) {
			 transaction.rollback();
		 }
		 return result;
	 }
	  
	
	 public void add(Usuario emp) {
		 EntityTransaction transaction = entityManager.getTransaction();
		 try {
			 transaction.begin();
			 entityManager.persist(emp);
			 transaction.commit();
		 } catch (Exception e) {
			 transaction.rollback();
		 }
	 }
	 
	 //El objeto debe ser previamente activado/consultado usando el mismo EntityManager antes de actualizarlo
	 public void update(Usuario emp) {
		 EntityTransaction transaction = entityManager.getTransaction();
		 try {
			 transaction.begin();
			 entityManager.merge(emp);
			 transaction.commit();
		 } catch (Exception e) {
			 transaction.rollback();
		 }
	 }
	 
	 
	//El objeto debe ser previamente activado/consultado usando el mismo EntityManager antes de borrarlo
	 public void delete(Usuario emp) {
		 EntityTransaction transaction = entityManager.getTransaction();
		 try {
			 transaction.begin();
			 entityManager.remove(emp);
			 transaction.commit();
		 } catch (Exception e) {
			 transaction.rollback();
		 }
	 }
	 

}
