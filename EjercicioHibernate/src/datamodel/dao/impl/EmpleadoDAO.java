package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.IEmpleadoDAO;
import datamodel.entities.Empleado;

public class EmpleadoDAO implements IEmpleadoDAO {

	private EntityManager entityManager;

	public EmpleadoDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Empleado getByName(String name) {
		Empleado empleado = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager.createQuery(
					"SELECT p FROM Empleado p  WHERE p.nombre = :name");
			query.setParameter("name", name);
			empleado = (Empleado)query.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return empleado;
	}
	
	public Empleado getById(int id) {
		Empleado empleado = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			empleado = (Empleado) entityManager.createQuery(
					"SELECT p FROM Empleado p  WHERE p.idUsuario = " + id)
					.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return empleado;
	}

	public List<Empleado> findAll() {
		List<Empleado> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p FROM Empleado p")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public void add(Empleado emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de actualizarlo
	public void update(Empleado emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.merge(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de borrarlo
	public void delete(Empleado emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

}
