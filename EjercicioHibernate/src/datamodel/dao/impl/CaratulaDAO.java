package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.ICaratulaDAO;
import datamodel.entities.Caratula;

public class CaratulaDAO implements ICaratulaDAO {

	private EntityManager entityManager;

	public CaratulaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	public Caratula getByCodigo(String codigo) {
		Caratula caratula = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager.createQuery(
					"SELECT p FROM Caratula p  WHERE p.codigo = :codigo");
			query.setParameter("codigo", codigo);
			caratula = (Caratula)query.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return caratula;
	}
	
	public Caratula getById(int id) {
		Caratula caratula = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			caratula = (Caratula) entityManager.createQuery(
					"SELECT p FROM Caratula p  WHERE p.id = " + id)
					.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return caratula;
	}

	public List<Caratula> findAll() {
		List<Caratula> result = new ArrayList<Caratula>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p FROM Caratula p")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public void add(Caratula caratula) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(caratula);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de actualizarlo
	public void update(Caratula caratula) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.merge(caratula);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de borrarlo
	public void delete(Caratula emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}
}
