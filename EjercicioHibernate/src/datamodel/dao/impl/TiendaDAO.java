package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.ITiendaDAO;
import datamodel.entities.Empleado;
import datamodel.entities.Tienda;

public class TiendaDAO implements ITiendaDAO {

	private EntityManager entityManager;

	public TiendaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Empleado> listEmpleadosTienda(String tienda) {
		List<Empleado> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager
					.createQuery("SELECT p FROM Empleado p join p.trabaja_en a WHERE a.nombre = :tienda");
			query.setParameter("tienda", tienda);
			result = query.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public Tienda getByName(String name) {
		Tienda tienda = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager
					.createQuery("SELECT p FROM Tienda p  WHERE p.nombre = :name");
			query.setParameter("name", name);
			tienda = (Tienda) query.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return tienda;
	}

	public Tienda getById(int id) {
		Tienda tienda = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			tienda = (Tienda) entityManager.createQuery(
					"SELECT p FROM Tienda p  WHERE p.idTienda = " + id)
					.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return tienda;
	}

	public List<Tienda> findAll() {
		List<Tienda> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p FROM Tienda p")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public void add(Tienda emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de actualizarlo
	public void update(Tienda emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.merge(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de borrarlo
	public void delete(Tienda emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

}
