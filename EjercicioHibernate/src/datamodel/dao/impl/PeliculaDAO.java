package datamodel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import datamodel.dao.IPeliculaDAO;
import datamodel.entities.Cliente;
import datamodel.entities.Pelicula;

public class PeliculaDAO implements IPeliculaDAO {

	private EntityManager entityManager;

	public PeliculaDAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public Pelicula getByName(String name) {
		Pelicula pelicula = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			Query query = entityManager.createQuery(
					"SELECT p FROM Pelicula p  WHERE p.nombre = :name");
			query.setParameter("name", name);
			pelicula = (Pelicula)query.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return pelicula;
	}
	
	public Pelicula getById(int id) {
		Pelicula pelicula = null;
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			pelicula = (Pelicula) entityManager.createQuery(
					"SELECT p FROM Pelicula p  WHERE p.idPelicula = " + id)
					.getSingleResult();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return pelicula;
	}

	public List<Pelicula> findAll() {
		List<Pelicula> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p FROM Pelicula p")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}

	public void add(Pelicula emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de actualizarlo
	public void update(Pelicula emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.merge(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}

	// El objeto debe ser previamente activado/consultado usando el mismo
	// EntityManager antes de borrarlo
	public void delete(Pelicula emp) {
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.remove(emp);
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
	}
	public List<Cliente> findClientes(Pelicula pel) {
		List<Cliente> result = new ArrayList<>();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			result = entityManager.createQuery("SELECT p.cliente_id FROM Cliente_pelicula p where p.pelicula_idpelicula= pel.idPelicula ")
					.getResultList();
			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		}
		return result;
	}
	
	
	
	

}
