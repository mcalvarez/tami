package datamodel.dao;

import java.util.List;

import datamodel.entities.Pelicula;

public interface IPeliculaDAO {
	Pelicula getByName(String name);
	List<Pelicula> findAll();
	void add(Pelicula pelicula);
	void update(Pelicula pelicula);
	void delete(Pelicula pelicula);
}
