import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import application.AltaDatos;
import application.Consultas;

public class Main {
	public static void main(String args[]) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("test_unit");
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();
		
		AltaDatos.inicializarDatos(entityManager);
		Consultas.ejecutar(entityManager);
		
		entityManager.close();
		entityManagerFactory.close();
	}
}
