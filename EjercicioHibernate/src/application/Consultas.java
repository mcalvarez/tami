package application;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import datamodel.dao.IClienteDAO;
import datamodel.dao.IPeliculaDAO;
import datamodel.dao.ITiendaDAO;
import datamodel.dao.impl.ClienteDAO;
import datamodel.dao.impl.PeliculaDAO;
import datamodel.dao.impl.TiendaDAO;
import datamodel.entities.Cliente;
import datamodel.entities.Empleado;
import datamodel.entities.Pelicula;
import datamodel.entities.Tienda;

/**
 * Consultas de ejemplo sobre la BD del videoclub
 */
public class Consultas {
	public static void ejecutar(EntityManager entityManager ) {
		IClienteDAO clienteDAO = new ClienteDAO(entityManager);
		ITiendaDAO tiendaDAO = new TiendaDAO(entityManager);
		IPeliculaDAO peliculaDAO = new PeliculaDAO(entityManager);

		// Consultar los empleados de una tienda
		Tienda nuevaTienda = tiendaDAO.getByName("Imdb");
		Set<Empleado> empleados = nuevaTienda.getEmpleados();
		
		// Mostramos el listado de 
		System.out.println("- Listar empleados de la tienda IMDB");
		listResults(empleados);
		
		// Recuperar las peliculas que alquilo un cliente
		Cliente cliente = clienteDAO.getByName("Paco");
		Set<Pelicula> peliculas = cliente.getAlquila();
		// Mostramos el listado de clientes
		listResults(peliculas);
		
		Pelicula pelicula = peliculaDAO.getByName("Alienado");

		// Recuperar los clientes que han alquilado una pelicula
		List<Cliente> clientes = clienteDAO.listClientesAlquilanPelicula(pelicula.getId());
		
		System.out.println("- Listar clientes que han alquilado una pelicula");
		// Mostramos el listado de clientes
		listResults(clientes);
	}
	
	public static <T> void listResults(Collection<T> results) {
		for (Object s : results) {
			System.out.println(s.toString());
		}
	}
}
