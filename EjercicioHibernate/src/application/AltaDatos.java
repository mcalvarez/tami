package application;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import datamodel.dao.ICaratulaDAO;
import datamodel.dao.IClienteDAO;
import datamodel.dao.IEmpleadoDAO;
import datamodel.dao.IPeliculaDAO;
import datamodel.dao.ITiendaDAO;
import datamodel.dao.impl.CaratulaDAO;
import datamodel.dao.impl.ClienteDAO;
import datamodel.dao.impl.EmpleadoDAO;
import datamodel.dao.impl.PeliculaDAO;
import datamodel.dao.impl.TiendaDAO;
import datamodel.entities.Caratula;
import datamodel.entities.Cliente;
import datamodel.entities.Empleado;
import datamodel.entities.Pelicula;
import datamodel.entities.Tienda;

/**
 * Clase donde se introducen los datos iniciales en la BD del VideoClub
 */
public class AltaDatos {	
	public static void inicializarDatos(EntityManager entityManager) {
		IEmpleadoDAO empleadoDAO = new EmpleadoDAO(entityManager);
		IClienteDAO clienteDAO = new ClienteDAO(entityManager);
		ITiendaDAO tiendaDAO = new TiendaDAO(entityManager);
		IPeliculaDAO peliculaDAO = new PeliculaDAO(entityManager);
		ICaratulaDAO caratulaDAO = new CaratulaDAO(entityManager);

		// Crear una tienda Imdb
		System.out.println("- Crear la tienda Imdb");
		Tienda nuevaTienda = new Tienda();
		nuevaTienda.setNombre("Imdb");
		tiendaDAO.add(nuevaTienda);

		// Obtener la tienda Imdb
		System.out.println("- Obtener la tienda Imdb");
		nuevaTienda = tiendaDAO.getByName("Imdb");

		// Crear los clientes
		System.out.println("- Crear el cliente Paco ");
		Cliente nuevoCliente = new Cliente();
		nuevoCliente.setNombre("Paco");
		clienteDAO.add(nuevoCliente);

		System.out.println("- Crear el cliente Luis");
		nuevoCliente = new Cliente();
		nuevoCliente.setNombre("Luis");
		clienteDAO.add(nuevoCliente);

		System.out.println("- Crear la cliente Jacinta");
		nuevoCliente = new Cliente();
		nuevoCliente.setNombre("Jacinta");
		clienteDAO.add(nuevoCliente);

		// Crear los empleados
		System.out.println("- Crear el empleado Billy");
		Empleado nuevoEmpleado = new Empleado();
		nuevoEmpleado.setNombre("Billy");
		nuevoEmpleado.setTienda(nuevaTienda);
		empleadoDAO.add(nuevoEmpleado);

		System.out.println("- Crear el empleado Joe");
		nuevoEmpleado = new Empleado();
		nuevoEmpleado.setNombre("Joe");
		nuevoEmpleado.setTienda(nuevaTienda);
		empleadoDAO.add(nuevoEmpleado);

		System.out.println("- Crear el empleado Luigi");
		nuevoEmpleado = new Empleado();
		nuevoEmpleado.setNombre("Luigi");
		nuevoEmpleado.setTienda(nuevaTienda);
		empleadoDAO.add(nuevoEmpleado);

		// Crear las caratulas
		Caratula caratula = new Caratula();
		caratula.setCodigo("12345");
		caratulaDAO.add(caratula);
		caratula = caratulaDAO.getByCodigo("12345");
		
		Caratula caratula2 = new Caratula();
		caratula2.setCodigo("56789");
		caratulaDAO.add(caratula2);
		caratula2 = caratulaDAO.getByCodigo("56789");
		
		Caratula caratula3 = new Caratula();
		caratula3.setCodigo("83958");
		caratulaDAO.add(caratula3);
		caratula3 = caratulaDAO.getByCodigo("83958");
		
		// Crear las peliculas
		System.out.println("- Crear la pelicula Alienado");
		Pelicula nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Alienado");
		nuevaPelicula.setCaratula(caratula);
		peliculaDAO.add(nuevaPelicula);

		System.out.println("- Crear la pelicula Desafio parcial");
		nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Desafio parcial");
		nuevaPelicula.setCaratula(caratula2);
		peliculaDAO.add(nuevaPelicula);

		System.out.println("- Crear la pelicula Titanico");
		nuevaPelicula = new Pelicula();
		nuevaPelicula.setNombre("Titanico");
		nuevaPelicula.setCaratula(caratula3);
		peliculaDAO.add(nuevaPelicula);

		// Recuperamos el empleado
		System.out.println("- Recuperamos el empleado");
		Empleado empleado = empleadoDAO.getByName("Titanico");

		// Alquilar una pelicula
		System.out.println("- Paco alquila las peliculas Alienado y Titanico");
		Cliente cliente = clienteDAO.getByName("Paco");
		Pelicula pelicula = peliculaDAO.getByName("Alienado");
		Pelicula pelicula2 = peliculaDAO.getByName("Titanico");

		Set<Pelicula> peliculas = new HashSet<Pelicula>();
		peliculas.add(pelicula);
		peliculas.add(pelicula2);
		cliente.setAlquila(peliculas);
		clienteDAO.update(cliente);
	}
}
